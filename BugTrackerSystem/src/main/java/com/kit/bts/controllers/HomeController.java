package com.kit.bts.controllers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.kit.bts.entities.Bug;
import com.kit.bts.entities.Comment;
import com.kit.bts.entities.Project;
import com.kit.bts.entities.User;
import com.kit.bts.service.BugService;
import com.kit.bts.service.CommentService;
import com.kit.bts.service.LanguageService;
import com.kit.bts.service.PhotoService;
import com.kit.bts.service.ProjectService;
import com.kit.bts.service.ReportService;
import com.kit.bts.service.UserService;
import com.kit.bts.service.UtilService;

@Controller
public class HomeController {
	
	@Autowired
	private UserService us;
	
	@Autowired
	private ProjectService ps;
	
	@Autowired
	private ReportService rs;
	
	@Autowired
	private BugService bs;
	
	@Autowired
	private LanguageService ls;

	@Autowired
	private CommentService cs;

	@Autowired
	private PhotoService phs;
	
	@Autowired
	@Qualifier("messageSource")
	private MessageSource messageSource;
	
	@Autowired
	@Qualifier("mailSender")
	private JavaMailSender mailSender;

	@Autowired
	@Qualifier("org.springframework.security.authenticationManager")
	private AuthenticationManager authenticationManager;
	
	@Autowired
	@Qualifier("jdbcUserService")
	public UserDetailsManager userDetailsManager;
	
	@Autowired
	private RememberMeServices rememberMeServices;
	
	private static final int REPORTS_SIZE = 12;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model, HttpServletRequest r) {
		
		boolean isAuthorized = true;
		try {
	        r.getSession();

	        UserDetails user = userDetailsManager.loadUserByUsername(r.getUserPrincipal().getName());
	        Authentication auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
	        SecurityContextHolder.getContext().setAuthentication(auth);
	    } catch (Exception e) {
	    	isAuthorized = false;
	    }
		
		return isAuthorized ? "redirect:/index" : "login";
	}
	
	@RequestMapping(value = "/login-form", method = RequestMethod.GET)
	public String loginForm(Model model) {
		
		return "loginPageForms/loginForm";
	}
	
	@RequestMapping(value = "/login-form", method = RequestMethod.POST)
	public @ResponseBody String loginPost(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		String login = UtilService.getQueryValueByKey(request, "login");
		String password = UtilService.getQueryValueByKey(request, "pass");
		
		String rememberMeStr = UtilService.getQueryValueByKey(request, "remem");
		boolean rememberMe = rememberMeStr.equals("true") || rememberMeStr.equals("on");
		
		if(us.getUserByCrendetials(login, password) == null) {
			return "error";
		} else {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
			token.setDetails(new WebAuthenticationDetails(request));                        
			Authentication authenticatedUser = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
			
			if(rememberMe) {
				rememberMeServices.loginSuccess(request, response, authenticatedUser);
			}
			
			return "redirect";
		}
	}
	
	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	public @ResponseBody int[] getStats() {

		return new int[] {us.getUsersQuantity(), ps.getProjectsQuantity()};
	}
	
	@RequestMapping(value = "/check-login", method = RequestMethod.GET)
	public @ResponseBody boolean checkLogin(HttpServletRequest request) {
		
		String login = UtilService.getQueryValueByKey(request, "data");
		
		if(login != null && !login.equals("")) {
			return us.isLoginEnabled(login);
		}
		
		return false;
	}
	
	@RequestMapping(value = "/check-mail", method = RequestMethod.GET)
	public @ResponseBody boolean checkMail(HttpServletRequest request) {
		
		String mail = UtilService.getQueryValueByKey(request, "data");
		
		if(mail != null && !mail.equals("")) {
			return us.isMailEnabled(mail);
		}
		
		return false;
	}
	
	@RequestMapping(value = "/bad-browser", method = RequestMethod.GET)
	public String badBrowserPage(Locale locale, Model model) {
		
		return "badBrowser";
	}
	
	@RequestMapping(value = "/registration-form", method = RequestMethod.GET)
	public String registrationForm(Locale locale, Model model) {
		
		model.addAttribute("newUser", new User());
		return "loginPageForms/registrationForm";
	}
	
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registrationPost(@ModelAttribute("newUser") User user) {
		
		us.save(user);
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/public-projects", method = RequestMethod.GET)
	public String publicProjectsPage(Model model, Locale l) {
		
		return "common/publicProjects";
	}
	
	// ====================================================================== index page
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String indexPage(Model model, Locale l) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		
		model.addAttribute("publicProjects", ps.getLastPublicProjects());
		
		if(rs.getNewReports(user, REPORTS_SIZE).size() < REPORTS_SIZE) {
			model.addAttribute("reports", rs.getLastNews(user, REPORTS_SIZE));
		} else {
			model.addAttribute("reports", rs.getNewReports(user, REPORTS_SIZE));
		}
		
		return "index";
	}
	
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public @ResponseBody void checkReports() {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		rs.changeStatusOfNewReports(user, REPORTS_SIZE);
	}
	
	// ====================================================================== projects page
	
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public String projectsPage(Model model, Locale l) {
		
		model.addAttribute("publicProjects", ps.getLastPublicProjects());
		return "projects";
	}
	
	@RequestMapping(value = "/projects/{login}", method = RequestMethod.GET)
	public String projectsUserPage(Model model, Locale l, @PathVariable(value = "login") String login, 
			HttpServletRequest r) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		String requestURI = r.getRequestURI();
		String decodedURI = "";
		
		try {
			decodedURI = URLDecoder.decode(requestURI, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String decodedLogin = decodedURI.substring(decodedURI.lastIndexOf("/") + 1, decodedURI.length());
		
		if(user.getLogin().equals(decodedLogin)) {
			return "redirect:/projects";
		}
		
		if(us.getUserByLogin(decodedLogin) == null) {
			return "redirect:/projects";
		}
		
		model.addAttribute("publicProjects", ps.getLastPublicProjects());
		return "projects";
	}
	
	@RequestMapping(value = "/get-projects", method = RequestMethod.GET)
	public @ResponseBody Set<Project> getUserProjects() {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		return user.getProjects();
	}
	
	@RequestMapping(value = "/get-projects/{login}", method = RequestMethod.GET)
	public @ResponseBody Set<Project> getUserProjects(@PathVariable(value = "login") String login,
			HttpServletRequest r) {
		
		String requestURI = r.getRequestURI();
		String decodedURI = "";
		
		try {
			decodedURI = URLDecoder.decode(requestURI, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String decodedLogin = decodedURI.substring(decodedURI.lastIndexOf("/") + 1, decodedURI.length());
		
		User user = us.getUserByLogin(decodedLogin);
		return us.getPublicProjects(user);
	}
	
	@RequestMapping(value = "/templates/pvt", method = RequestMethod.GET)
	public String pvtPage() {
		
		return "templates/projectViewTemplate";
	}
	
	@RequestMapping(value = "/add-project/{projectId}", method = RequestMethod.GET)
	public String addProjectForm(@PathVariable(value = "projectId") int projectId, Model model) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("owner", user);
		
		if(projectId <= 0) {
			Project p = new Project();
			p.getUsers().add(user);

			model.addAttribute("newProject", p);
		} else {
			model.addAttribute("newProject", ps.findById(projectId));
		}
		
		model.addAttribute("langs", ls.getAllEntity());
		
		return "projectsPageForms/addProjectForm";
	}
	
	@RequestMapping(value = "/add-project", method = RequestMethod.POST)
	public String addProjectPost(@ModelAttribute(value = "newProject") Project project,
			HttpServletRequest request, Locale l, @RequestParam(value = "ids") int[] ids) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		ps.setUsersToProject(project, ids);

		if(project.getProjectId() == 0) {
			ps.save(project, l, user);
		} else {
			ps.update(project);
		}
		
		return "redirect:/projects";
	}
	
	@RequestMapping(value = "/delete-project/{prId}", method = RequestMethod.POST)
	public String deleteProject(@PathVariable(value = "prId") int prId, Locale l) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		Project p = ps.findById(prId);
		
		if(p != null && p.getUsers().contains(user)) {
			ps.delete(p, l, user);
		}
		
		return "redirect:/projects";
	}
	
	@RequestMapping(value = "/get-user", method = RequestMethod.GET)
	public @ResponseBody List<User> getUsersByNameAndSname(HttpServletRequest r) {
		
		String key = "query";
		String value = UtilService.getQueryValueByKey(r, key);
		
		return us.getUsersByNameAndSname(value);
	}

	@RequestMapping(value = "/show-project/{prId}", method = RequestMethod.GET)
	public String getProjectPage(@PathVariable(value = "prId") int prId, Model model) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		Project p = ps.findById(prId);
		
		if(p == null) {
			model.addAttribute("addBug", 0);
		}
		
		if(p != null && p.getUsers().contains(user)) {
			model.addAttribute("bugs", p.getBugs());
			model.addAttribute("project", p);
		}
		
		return "showProject";
	}

	@RequestMapping(value = "/get-project/{prId}", method = RequestMethod.GET)
	public @ResponseBody Project getProject(@PathVariable(value = "prId") int prId, Model model) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		Project project = ps.findById(prId);
		
		if(project != null) {
			if(project.getPrivacy().equals("Public")) {
				return project;
			} else {
				if(project.getUsers().contains(user)) {
					return project;
				} 
			}
		}
		
		return new Project();
	}
	
	@RequestMapping(value = "/templates/bt", method = RequestMethod.GET)
	public String btPage() {
		
		return "templates/bugTemplate";
	}
	
	// =========================================================================== bugs
	@RequestMapping(value = "/add-bug/{bugId}", method = RequestMethod.GET)
	public String addBugForm(@PathVariable(value = "bugId") int bugId, Model model,
			@RequestParam("p") int projectId) {
		
		if(bugId <= 0) {
			Bug b = new Bug();
			b.setProject(ps.findById(projectId));

			model.addAttribute("newBug", b);
		} else {
			model.addAttribute("newBug", bs.findById(bugId));
		}
		
		return "projectsPageForms/addBugForm";
	}
	
	@RequestMapping(value = "/add-bug", method = RequestMethod.POST)
	public String addProjectPost(@ModelAttribute(value = "newBug") Bug bug,
			HttpServletRequest request, Locale l, @RequestParam(value = "imgs") MultipartFile[] imgs,
			@RequestParam(value = "pid") int pid) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		
		bug.setProject(ps.findById(pid));
		bs.save(bug, l, user, mailSender);
		
		if(imgs != null && imgs.length > 0) {
			phs.addPhotosToBug(imgs, bug);
		}
		
		return "redirect:/show-project/" + pid;
	}
	
	@RequestMapping(value = "/show-bug/{bugId}", method = RequestMethod.GET)
	public String showBug(@PathVariable(value = "bugId") int bugId, Model model) {
		
		User user = us.getUserByLogin(SecurityContextHolder.getContext().getAuthentication().getName());
		Bug bug = bs.findById(bugId);
		
		if(bugId > 0) {
			model.addAttribute("bug", bug);
		} 
		
		if(bug.getProject().getUsers().contains(user) || user.getLogin().equals(bug.getOwner().getLogin())) {
			model.addAttribute("changeStatus", "true");
		}
		
		model.addAttribute("newComment", new Comment());
		
		return "showBug";
	}
	
	@RequestMapping(value = "/delete-bug/{bugId}", method = RequestMethod.GET)
	public String deleteBug(@PathVariable(value = "bugId") int bugId, Model model) {
		
		Bug bug = bs.findById(bugId);
		int projectId = bug.getProject().getProjectId();
		
		if(bugId > 0) {
			bs.delete(bug);
		} 
		
		return "redirect:/show-project/" + projectId;
	}
	
	@RequestMapping(value = "/change-status", method = RequestMethod.POST)
	public @ResponseBody String changeStatus(@RequestParam(value = "new-status") String status, 
			@RequestParam(value = "bugId") int bugId) {
		
		Bug bug = bs.findById(bugId);
		bug.setStatus(status);
		bs.update(bug);
		return status;
	}
	
	@RequestMapping(value = "/save-comment", method = RequestMethod.POST)
	public String saveComment(@ModelAttribute("newComment") Comment comment, 
			@RequestParam(value = "bugId") int bugId) {
		
		comment.setBug(bs.findById(bugId));
		cs.save(comment);
		
		return "redirect:/show-bug/" + bugId;
	}
}
