package com.kit.bts.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonBackReference;

@Entity
@Table(name = "photos")
public class Photo {
	
	@Id
	@GeneratedValue
	@Column(name = "photo_id")
	private int photoId;
	
	@Column(name = "link")
	private String link;
	
	@ManyToOne
	@JoinColumn(name = "bug_id")
	@JsonBackReference
	private Bug bug;

	public int getPhotoId() {
		return photoId;
	}

	public void setPhotoId(int photoId) {
		this.photoId = photoId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Bug getBug() {
		return bug;
	}

	public void setBug(Bug bug) {
		this.bug = bug;
	}
}
