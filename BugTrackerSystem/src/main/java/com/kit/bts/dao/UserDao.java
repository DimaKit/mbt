package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.User;

@Repository
public class UserDao extends AbstractDaoImpl<User, Integer> {
	
	public UserDao() {
		super(User.class);
	}

}
