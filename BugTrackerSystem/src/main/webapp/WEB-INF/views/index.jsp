<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><spring:message code="label.indexPageTitle" /></title>

<link href="<c:url value='/resources/css/bootstrap.min.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/indexPage.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/common.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/font-awesome.min.css' />"
	rel="stylesheet">

<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<noscript>
		<meta http-equiv="refresh" content="0;url=/bts/bad-browser">
	</noscript>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="common/header.jspf"%>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 news">
				<h4 class="h4">
					<spring:message code="label.reports" />
					<i title="<spring:message code="label.setAsWatched" />"
						class="fa fa-check pull-right check-reports"></i>
				</h4>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<c:choose>
						<c:when test="${reports.size() == 0}">
							<p class="new-reports">
								<spring:message code="label.noReportsYet" />
							</p>
						</c:when>
						<c:otherwise>
							<c:forEach items="${reports}" var="report">
								<c:choose>
									<c:when test="${report.status eq 'new'}">
										<p class="new-reports">${report.text}</p>
									</c:when>
									<c:otherwise>
										<p class="watched-reports">${report.text}</p>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 public-projects">
				<%@ include file="common/publicProjects.jsp"%>
			</div>
		</div>

		<div class="row">
			<%@ include file="common/footer.jspf"%>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<c:url value='/resources/js/jquery-1.11.2.min.js' />"></script>
	<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/resources/js/indexPage.js' />"></script>
</body>
</html>