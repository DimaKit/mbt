<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add-bug">

	<form:form method="post" modelAttribute="newBug" action="/bts/add-bug" enctype="multipart/form-data">
		<form:input path="bugId" type="hidden" id="bid" />
		<form:input path="created" type="hidden" />
		<input name="pid" type="hidden" value="${newBug.project.projectId}" />
	
		<div class="form-group">
			<label for="summary"><spring:message code='label.summary' /></label>
			<form:input type="text" path="summary" id="summary" name="summary"
				class="form-control" />
		</div>

		<div class="form-group">
			<label for="description"><spring:message code='label.bugDesc' /></label>
			<form:textarea type="text" path="description" id="description"
				name="description" rows="5" class="form-control"
				style="resize: none" />
		</div>
		
		<div class="form-group">
			<label for="priority"><spring:message code='label.priority' /></label>
			<form:select path="priority" id="priority" name="priority" class="form-control">
				<form:option value="Info" class="form-control"><spring:message code='label.priority.info' /></form:option>
				<form:option value="Minor" class="form-control"><spring:message code='label.priority.minor' /></form:option>
				<form:option value="Major" class="form-control"><spring:message code='label.priority.major' /></form:option>
				<form:option value="Critical" class="form-control"><spring:message code='label.priority.critical' /></form:option>
				<form:option value="Blocker" class="form-control"><spring:message code='label.priority.blocker' /></form:option>
			</form:select>
		</div>
		
		<div class="form-group">
			<label for="images"><spring:message code='label.addPhotos' /></label>
			<input type="file" name="imgs" id="images" multiple="multiple" accept="image/*" />
			<br clear="all">
			
			<div class="photo-preview"></div>
			<br clear="all">
		</div>
	
		<input type="submit" value="<spring:message code='label.confirm' />" class="btn btn-primary btn-block" />
	</form:form>
</div>
