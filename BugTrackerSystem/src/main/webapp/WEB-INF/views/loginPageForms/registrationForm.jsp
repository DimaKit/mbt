<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 registration-form">
	<h4 class="h4">
		<spring:message code='label.registerNewUser' />
		<i class="fa fa-sign-in switch-form"
			onclick="switchForm('login-form');"></i>
	</h4>
	<hr>
	<form:form method="POST" action="registration" id="reg-form"
		modelAttribute="newUser">
		<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
			<label for="login"><spring:message code="label.login" /> *</label>
			<form:input path="login" type="text" name="login"
				class="form-control" id="login" autofocus="autofocus" />

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
				<p>
					<spring:message code="label.cantBeEmpty" />
				</p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error ue">
				<p>
					<spring:message code='label.userExists' />
				</p>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
			<label for="pass"><spring:message code="label.password" /> *</label>
			<form:input path="password" type="password" name="password"
				class="form-control" id="pass" />

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
				<p>
					<spring:message code="label.cantBeEmpty" />
				</p>
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<label for="mail"><spring:message code="label.mail" /> *</label>
			<form:input path="mail" type="email" name="mail" class="form-control"
				id="mail" />

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
				<p>
					<spring:message code="label.cantBeEmpty" />
				</p>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error ue">
				<p>
					<spring:message code='label.mailExists' />
				</p>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
			<label for="name"><spring:message code="label.name" /></label>
			<form:input path="name" type="text" name="name" class="form-control"
				id="name" />
		</div>

		<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
			<label for="sname"><spring:message code="label.sname" /></label>
			<form:input path="sname" type="text" name="sname"
				class="form-control" id="sname" />
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 push-down">
			<input type="submit" class="btn btn-success btn-block"
				value="<spring:message code="label.confirm" />" id="confirm">
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 note">
			<spring:message code="label.note" />
		</div>
	</form:form>
</div>
