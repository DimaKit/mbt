package com.kit.bts.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.CommentDao;
import com.kit.bts.entities.Comment;

@Service
@Transactional
public class CommentService {
	
	@Autowired
	private CommentDao dao;
	
	public Comment findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Comment e) {
		e.setCreated(new Date());
		dao.save(e);
	}

	public void update(Comment e) {
		dao.update(e);
	}

	public void delete(Comment e) {
		dao.delete(e);
	}

	public List<Comment> getAllEntity() {
		return dao.getAllEntity();
	}
}
