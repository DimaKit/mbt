package com.kit.bts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.kit.bts.dao.PhotoDao;
import com.kit.bts.entities.Bug;
import com.kit.bts.entities.Photo;

@Service
@Transactional
public class PhotoService {
	
	@Autowired
	private PhotoDao dao;
	
	private String[] extentions = {".jpg", ".jpeg", ".png", ".bmp", ".gif"};
	
	public Photo findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Photo e) {
		dao.save(e);
	}

	public void update(Photo e) {
		dao.update(e);
	}

	public void delete(Photo e) {
		UtilService.deletePhoto(e);
		dao.delete(e);
	}

	public List<Photo> getAllEntity() {
		return dao.getAllEntity();
	}
	
	public void addPhotosToBug(MultipartFile[] images, Bug bug) {
		for(MultipartFile f: images) {
			if(isPhoto(f)) {
				String link = UtilService.savePhoto(f);
				Photo p = new Photo();
				p.setLink(link);
				p.setBug(bug);
				save(p);
			}
		}
	}
	
	private boolean isPhoto(MultipartFile file) {
		for (String s: extentions) {
			if(file.getOriginalFilename().endsWith(s)) {
				return true;
			}
		} 
		
		return false;
	}
}
