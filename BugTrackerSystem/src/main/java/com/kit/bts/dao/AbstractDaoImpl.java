package com.kit.bts.dao;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;

@SuppressWarnings("unchecked")
public abstract class AbstractDaoImpl<E, I extends Serializable> implements AbstractDao<E, I> {
	
	@Autowired
	protected SessionFactory sessionFactory;
	
	private Class<E> entityClass;
	
	public AbstractDaoImpl(Class<E> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public E findById(I id) {
		return (E) sessionFactory.getCurrentSession().get(entityClass, id);
	}

	@Override
	public void save(E e) {
		sessionFactory.getCurrentSession().save(e);
	}
	
	@Override
	public void update(E e) {
		sessionFactory.getCurrentSession().update(e);
	}

	@Override
	public void delete(E e) {
		sessionFactory.getCurrentSession().delete(e);
	}

	@Override
	public List<E> getAllEntity() {
		return sessionFactory.getCurrentSession().createQuery(
				"from " + entityClass.getSimpleName()).list();
	}

	@Override
	public Set<E> executeCriterionSet(Criterion criterion, Order order, int maxResults) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass);
		criteria.add(criterion);
		
		if(order != null) {
			criteria.addOrder(order);
		}
		
		if(maxResults != 0) {
			criteria.setMaxResults(maxResults);
		}
		
		Set<E> set = new HashSet<E>();
		set.addAll(criteria.list());
		return set;
	}
	
	@Override
	public List<E> executeCriterionList(Criterion criterion, Order order, int maxResults) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entityClass);
		
		if(order != null) {
			criteria.addOrder(order);
		}
		
		if(maxResults != 0) {
			criteria.setMaxResults(maxResults);
		}
		
		return criteria.add(criterion).list();
	}
}

