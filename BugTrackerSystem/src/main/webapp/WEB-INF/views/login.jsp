<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><spring:message code='label.loginPageTitle' /></title>

<link href="<c:url value='/resources/css/bootstrap.min.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/loginPage.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/common.css' />"
	rel="stylesheet">
<link href="<c:url value='/resources/css/font-awesome.min.css' />"
	rel="stylesheet">

<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<noscript>
		<meta http-equiv="refresh" content="0;url=/bts/bad-browser">
	</noscript>
	<div class="container-fluid">
		<div class="row">
			<div
				class="col-xs-12 col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-9 col-lg-2 col-lg-offset-10 langs">
				<spring:message code="label.langInfo" />
				<select
					onchange="location = this.options[this.selectedIndex].value;"
					class="form-control">
					<option disabled selected><spring:message
							code="label.selectLang" /></option>
					<option value="?locale=ua">Українська</option>
					<option value="?locale=ru">Русский</option>
					<option value="?locale=en">English</option>
				</select>
			</div>
		</div>
		<div class="row">
			<div
				class="col-lg-8 col-md-8 vert col-sm-12 col-xs-12 left-container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slider">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3 class="title">
								<spring:message code="label.sliderH1" />
							</h3>
							<h3 class="title hidden">
								<spring:message code="label.sliderH2" />
							</h3>
							<h3 class="title hidden">
								<spring:message code="label.sliderH3" />
							</h3>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<img src="<c:url value='/resources/img/simple.png' />"
								class="img-responsive img-rounded image" alt="benefit1"> <img
								src="<c:url value='/resources/img/usability.png' />"
								class="img-responsive img-rounded image hidden" alt="benefit2">
							<img src="<c:url value='/resources/img/cross-testing.png' />"
								class="img-responsive img-rounded image hidden" alt="benefit3">
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<span class="arguments"><spring:message
									code="label.sliderA1" /></span> <span class="arguments hidden"><spring:message
									code="label.sliderA2" /></span> <span class="arguments hidden"><spring:message
									code="label.sliderA3" /></span>
						</div>
					</div>
				</div>

				<div class="row">
					<div
						class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12 stats">
						<table>
							<tr>
								<td><h4>
										<spring:message code='label.usInSystem' />
									</h4></td>
								<td class="us-val"></td>
							</tr>
							<tr>
								<td><h4>
										<spring:message code='label.prInSystem' />
									</h4></td>
								<td class="pr-val"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<!-- 
    		-->
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form vert">
				<%@ include file="loginPageForms/loginForm.jsp"%>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="<c:url value='/resources/js/jquery-1.11.2.min.js' />"></script>
	<script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/resources/js/loginPage.js' />"></script>
	<script src="<c:url value='/resources/js/loginFormValidator.js' />"></script>
	<script src="<c:url value='/resources/js/registerFormValidator.js' />"></script>
</body>
</html>