package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Project;

@Repository
public class ProjectDao extends AbstractDaoImpl<Project, Integer> {
	
	public ProjectDao() {
		super(Project.class);
	}
	
}
