package com.kit.bts.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.google.gdata.client.photos.PicasawebService;
import com.google.gdata.data.media.MediaFileSource;
import com.google.gdata.data.photos.GphotoEntry;
import com.google.gdata.data.photos.PhotoEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.kit.bts.entities.Photo;

public class UtilService {
	private static final String mail     = "v3il@ukr.net",
								password = "28121994",
								userId   = "118344448774234396275", 
								albumId  = "6117496320686090593";
	
	public static String md5Encrypt(String s) {
		StringBuilder hexString = new StringBuilder();
		
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			md.update(s.getBytes());
			
			byte[] hash = md.digest();

			for (int i = 0; i < hash.length; i++) {
	            if ((0xff & hash[i]) < 0x10) {
	                hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
	            }
	            else {
	                hexString.append(Integer.toHexString(0xFF & hash[i]));
	            }
	        }
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return hexString.toString();
	}
	
	private static PicasawebService authentication() {
		PicasawebService myService = new PicasawebService("bts");
		
		try {
			myService.setUserCredentials(mail, password);
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}
		
		return myService;
	}
	
	public static String savePhoto(MultipartFile file) {
		PicasawebService ps = authentication();
		File convFile = new File(file.getOriginalFilename());
		String url = "https://picasaweb.google.com/data/feed/api/user/" + userId + "/albumid/" + albumId;
		MediaFileSource myMedia = new MediaFileSource(convFile, "image/png");
		
		try {
			file.transferTo(convFile);
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		PhotoEntry returnedPhoto = null;
		
		try {
			returnedPhoto = ps.insert(new URL(url), PhotoEntry.class, myMedia);
			System.out.println(returnedPhoto.getHtmlLink().getHref());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		return returnedPhoto.getMediaContents().get(0).getUrl();
	}
	
	public static void deletePhoto(Photo p) {
		PicasawebService ps = authentication();
		PhotoEntry pe = null;
		
		try {
			pe = (PhotoEntry) ps.getEntry(new URL(p.getLink()), GphotoEntry.class).getAdaptedEntry();
			pe.delete();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
	
	public static String getQueryValueByKey(HttpServletRequest r, String key) {
		
		String[] p = null;
		try {
			p = URLDecoder.decode(r.getQueryString(), "UTF-8").split("&");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		if(p != null && p.length != 0) {
			for(String pair: p) {
				if(pair.startsWith(key)) {
					return pair.substring(pair.indexOf("=") + 1, pair.length());
				}
			}
		}
		
		return null;
	}
}
