package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Report;

@Repository
public class ReportDao extends AbstractDaoImpl<Report, Integer> {
	
	public ReportDao() {
		super(Report.class);
	}
	
}
