package com.kit.bts.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.BugDao;
import com.kit.bts.entities.Bug;
import com.kit.bts.entities.Comment;
import com.kit.bts.entities.Photo;
import com.kit.bts.entities.User;

@Service
@Transactional
public class BugService {
	
	@Autowired
	private BugDao dao;
	
	@Autowired
	private ReportService rs;

	@Autowired
	private PhotoService ps;

	@Autowired
	private CommentService cs;
	
	public Bug findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Bug e, Locale locale, User user, MailSender sender) {
		e.setCreated(new Date());
		e.setOwner(user);
		e.setStatus("New");
		dao.save(e);
		
		rs.createReportOnBugCreating(e, locale, user, sender);
	}

	public void update(Bug e) {
		dao.update(e);
	}

	public void delete(Bug e) {
		
		for(Photo p: e.getPhotos()) {
			ps.delete(p);
		}

		for(Comment c: e.getComments()) {
			cs.delete(c);
		}
		
		dao.delete(e);
	}

	public List<Bug> getAllEntity() {
		return dao.getAllEntity();
	}
}
