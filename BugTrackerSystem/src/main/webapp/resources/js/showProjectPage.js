jQuery(function($) {
	var urlPath = window.location.href.split('/');
	var id = urlPath[urlPath.length - 1];
	var fields = {
			"name": "text", 
			"description": "text", 
			"privacy": "text", 
			"created": "date", 
			"webSite": "{webSite}", 
			"language": "image", 
			"company": "text"
		},
	    container = $(".projects-data"),
	    templateURL = "/bts/templates/pvt";
	
	$.getJSON("/bts/get-project/" + id, function(project) {
		if(project["projectId"] === 0) {
			container.find(".tip").removeClass("hidden");
		} else {
			fillTemplate(container, templateURL, project, fields);
		}
	});
	
	$(".add-bug").click(function() {
		addBug($(this));
	});
	
	$(".search-bugs").keyup(function() {
		$(".bugs-table tbody tr").show();
		$('.bugs-table tbody tr:not(:contains(' + $(this).val() + '))').hide();
	});
	
	$(".cancel-filter").click(function() {
		$(".search-bugs").val("");
		$(".bugs-table tbody tr").show();
	});
});

var addBug = function(container) {
	$(".modal-body").load(container.attr("data-url"), function(data) {
		$('#bug-modal').modal('show');
		
		$("#images").change(function(e) {
			if(!e.target.files || !window.FileReader) return;

			$(".photo-preview").empty();
			
			var files = e.target.files;
			var filesArr = Array.prototype.slice.call(files);
			filesArr.forEach(function(f, i) {
				var f = files[i];
				if(!f.type.match("image.*")) {
					return;
				}

				var reader = new FileReader();
				reader.onload = function (e) {
					var html = "<img src='" + e.target.result + "' class='img-responsive' />";
					$(".photo-preview").prepend("<div class='col-md-3 photo'></div>");
			        $(".photo:first").prepend(html);
				};
				reader.readAsDataURL(f); 
			});
		});

	});
};

var fillTemplate = function(container, templateURL, entity, fields, key) {
	$.get(templateURL, packData);

	function packData(data) {
		container.append(data);
		
		$.each(fields, function(key, val) {
			if(entity.hasOwnProperty(key)) {
				if(val === "text") {
					$(".single-project:last div." + key).append(" " + entity[key]);
				}
				
				if(val === "image") {
					var pathToImg = "/bts/resources/img/" + entity[key] + ".png";
					$(".single-project:last div." + key).html(
						"<img alt='" + entity[key] + "' src='" + pathToImg + "' class='img-responsive img-rounded'>"
					);
				}
				
				if(val.indexOf("/") === 0 || val.indexOf("{") === 0) {
					var pathVar = val.substring(val.indexOf("{") + 1, val.lastIndexOf("}"));
					var href = "<a href='" + val.replace(val.substring(val.indexOf("{"), val.length), entity[pathVar]) + 
							   "' class='dark'>" + entity[key] + "</a>";
					$(".single-project:last div." + key).html(href);
				}

				if(val === "date") {
					$(".single-project:last div." + key).append(
						 " " + new Date(entity[key]).toLocaleDateString());
				}
			}
		});
	}
};
