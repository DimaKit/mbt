package com.kit.bts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.LanguageDao;
import com.kit.bts.entities.Language;

@Service
@Transactional
public class LanguageService {
	
	@Autowired
	private LanguageDao dao;
	
	public Language findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Language e) {
		dao.save(e);
	}

	public void update(Language e) {
		dao.update(e);
	}

	public void delete(Language e) {
		dao.delete(e);
	}

	public List<Language> getAllEntity() {
		return dao.getAllEntity();
	}
}
