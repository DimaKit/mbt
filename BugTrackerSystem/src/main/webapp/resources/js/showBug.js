$(document).ready(function() {
	$("#cs").click(function(e) {
		e.preventDefault();
		$.ajax({
			url : "/bts/change-status?new-status=" + $("#ns").val() + "&bugId=" + $("#bugId").val(),
			method : "post",
			success : function() {
				$("#ns").css("background", "#D9FFCB");
				
				setTimeout(function() {
					$("#ns").animate({'background-color': 'white'}, 'slow');
				}, 2000);
			}
		});
	});
	
	$(".fancybox").fancybox({
		overlayColor: 'rgba(0, 0, 0, 0.7)'
	});
	
	$("#ns option[value=" + $("#bugStatus").val() + "]").attr('selected','selected');
});