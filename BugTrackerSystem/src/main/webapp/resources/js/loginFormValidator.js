function getValidator() {
	var val = {
		'login' : function() {
			$(".error:first").fadeOut(300);
			var ele = $('#login');
			
			if(ele.val() == "") {
				val.errors = true;
				ele.nextAll(".error:first").fadeIn(300);
			} else {
				ele.nextAll(".error:first").fadeOut(300);
			}
		},
		
		'password' : function() {
			$(".error:first").fadeOut(300);
			var ele = $('#pass');
			
			if(ele.val() == "") {
				val.errors = true;
				ele.nextAll(".error:first").fadeIn(300);
			} else {
				ele.nextAll(".error:first").fadeOut(300);
			}
		},
		
		'sendIt' : function (e) {
			if(!$(this).errors) {
				$.ajax({
		            type: "post",
		            url: "login-form?login=" + $("#login").val() + "&pass=" + $("#pass").val() + "&remem=" + $("#remem").is(":checked"),
		            data: "",
		            success: function(data) {
		            	if(data == "error") {
		            		$(".error:first").fadeIn(300);
		            	}
		            	
		            	if(data == "redirect") {
		            		location.href = "/bts/index";
		            	} 
		            },
		        });
				e.preventDefault();
			} else {
				$(".error:first").fadeIn(300);
				e.preventDefault();
			}
		}
	};
	
	return val;
}