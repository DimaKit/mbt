<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><spring:message code="label.showProjectPageTitle" /></title>
	
	<link href="<c:url value='/resources/css/bootstrap.min.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/showBugPage.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/common.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/jquery.fancybox.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/font-awesome.min.css' />" rel="stylesheet">
	
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>
  <body>
  	<noscript>
  		<meta http-equiv="refresh" content="0;url=/bts/bad-browser">
	</noscript>
  	<div class="container-fluid">
  		<div class="row">
  			<%@ include file="common/header.jspf" %>	
  		</div>
  		
  		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cont">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bug">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title">
						<h3><spring:message code='label.bug' /> №${bug.bugId} | 
							<span class="${bug.priority}">${bug.priority}</span>
							<small class="pull-right text-muted">
								<fmt:formatDate pattern="dd.MM.yyyy HH:mm:ss" value="${bug.created}" />
							</small>
						</h3>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 left">
						<input type="hidden" value="${bug.bugId}" id="bugId">
						<input type="hidden" value="${bug.status}" id="bugStatus">
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p><spring:message code='label.summary' />: ${bug.summary}</p>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p><spring:message code='label.bugDesc' />: ${bug.description}</p>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<p><spring:message code='label.owner' />: <a href="/bts/projects/${bug.owner.login}" class="dark">${bug.owner.login}</a></p>
						</div>
						
						<c:if test="${not empty bug.photos}">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 photos">
								<p><spring:message code='label.attachments' />:</p>
								<c:forEach items="${bug.photos}" var="photo">
									<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
										<a class="fancybox" rel="gallery1" href="${photo.link}">
											<img src="${photo.link}" class="img-responsive img-rounded" />
										</a>
									</div>
								</c:forEach>
							</div>
						</c:if>
					</div>
					
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<c:if test="${pageContext.request.userPrincipal.name eq bug.owner.login}">
							<a href="/bts/delete-bug/${bug.bugId}" class="btn btn-primary btn-block">
								<spring:message code='label.deleteBug' />
							</a><hr>
						</c:if>
						
						<c:choose>
							<c:when test="${not empty changeStatus}">
								<div class="form-group">
									<label for="ns"><spring:message code='label.changeStatus' /></label>
									
									<select name="new-status" class="form-control" id="ns">
										<option value="New" disabled="disabled"><spring:message code='label.status.new' /></option>
										<option value="Cannot_verify"><spring:message code='label.status.cv' /></option>
										<option value="Could_not_dublicate"><spring:message code='label.status.cnd' /></option>
										<option value="Deferrer"><spring:message code='label.status.def' /></option>
										<option value="Delegated"><spring:message code='label.status.del' /></option>
										<option value="Fixed"><spring:message code='label.status.fixed' /></option>
										<option value="In_process"><spring:message code='label.status.ip' /></option>
										<option value="No_relevant"><spring:message code='label.status.nr' /></option>
										<option value="Re-do"><spring:message code='label.status.rd' /></option>
										<option value="Searching_conditions"><spring:message code='label.status.sc' /></option>
										<option value="Closed"><spring:message code='label.status.closed' /></option>
										<option value="Not_a_bug"><spring:message code='label.status.nab' /></option>
									</select>
								</div>
								
								<a href="#" class="btn btn-primary btn-block" id="cs"><spring:message code='label.confirm' /></a>
							</c:when>
							<c:otherwise>
								<p>${bug.status}</p>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 comments">
					<h4><spring:message code='label.comments' /></h4>
					
					<form:form modelAttribute="newComment" method="post" action="/bts/save-comment?bugId=${bug.bugId}">
						<div class="form-group">
							<label for="comment-text"><spring:message code='label.yourCommentLabel' /></label>
							<form:textarea path="comment" class="form-control comment-text" id="comment-text"></form:textarea>
						</div>
						<input type="submit" id="save-comment" class="btn btn-primary" 
							value="<spring:message code='label.confirm' />" style="margin-bottom: 5px;">
					</form:form>
					
					<table class="table">
						<c:choose>
							<c:when test="${not empty bug.comments}">
								<c:forEach items="${bug.comments}" var="comment">
									<tr>
										<td width="15%">
											<fmt:formatDate pattern="dd.MM.yyyy HH:mm:ss" value="${comment.created}" /><br>
											<p>
												<a href="/bts/projects/${comment.username}" class="dark">${comment.username}</a> <spring:message code='label.said' />:
											</p>
										</td>
										<td>
											<p>${comment.comment}</p>
										</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr><td><spring:message code='label.noCommentsYet' /></td></tr>
							</c:otherwise>
						</c:choose>
					</table>
				</div>
			</div>
  		</div>
  		
  		<div class="row">
  			<%@ include file="common/footer.jspf" %>
  		</div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/jquery-1.11.2.min.js' />"></script>
    <script src="<c:url value='/resources/js/jquery.autocomplete.js' />"></script>
    <script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
    <script src="<c:url value='/resources/js/showBug.js' />"></script>
    <script src="<c:url value='/resources/js/jquery.fancybox.pack.js' />"></script>
    <script src="<c:url value='/resources/js/jquery.mousewheel-3.0.6.pack.js' />"></script>
    <script src="<c:url value='/resources/js/jquery.color-2.1.2.min.js' />"></script>
  </body>
</html>