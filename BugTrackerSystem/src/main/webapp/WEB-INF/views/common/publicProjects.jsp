<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h4 class="h4">
	<spring:message code="label.publicProjects" />
</h4>

<table class="table">
	<c:forEach items="${publicProjects}" var="project">
		<tr>
			<td width="60%" class="bold"><a href="/bts/show-project/${project.projectId}">${project.name}</a></td>
			<td>${project.language}</td>
		</tr>
		<tr class="with-border">
			<td><span class="fa fa-user"></span> <a href="/bts/projects/${project.users.get(0).login}">${project.users.get(0).login}</a></td>
			<td><span class="fa fa-clock-o"></span> <fmt:formatDate
					pattern="dd.MM.yyyy" value="${project.created}" /></td>
		</tr>
	</c:forEach>
</table>