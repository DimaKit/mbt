package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Comment;

@Repository
public class CommentDao extends AbstractDaoImpl<Comment, Integer> {
	
	public CommentDao() {
		super(Comment.class);
	}
	
}
