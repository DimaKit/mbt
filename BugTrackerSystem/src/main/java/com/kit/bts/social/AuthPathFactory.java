package com.kit.bts.social;

import org.springframework.stereotype.Service;

@Service
public class AuthPathFactory {
	
	private String vkAppId = "4704551";
	private String vkSecret = "Qn5Ix4lsui2QbKvAFVYV";
	private String vkRedirectUri = "http://localhost:8080/project/vk";
	
	private String fbAppId = "634772936667915";
	private String fbSecret = "b63325402e5d23c16b3f33c2195ec37b";
	private String fbRedirectUri = "http://localhost:8080/project/fb";
	
	private String gpAppId = "750052618798-gbl7du1dn2hltj69031dseh4qekl66nv";
	private String gpSecret = "qzVataz9aQFePvMd_leD38H5";
	private String gpRedirectUri = "http://localhost:8080/project/gp";
	
	public String getCodePath(String tag) {
		if(tag.equals("vk")) {
			return "https://oauth.vk.com/authorize?client_id=" + 
					vkAppId + "&scope=notify&redirect_uri=" + 
					vkRedirectUri + "&response_type=code&v=5.27";
		}
		
		if(tag.equals("fb")) {
			return "https://www.facebook.com/dialog/oauth?client_id=" + 
					fbAppId + "&redirect_uri=" + fbRedirectUri + "&response_type=code";
		}
		
		if(tag.equals("gp")) {
			return "https://accounts.google.com/o/oauth2/auth?client_id=" + 
					gpAppId + "&scope=https://www.googleapis.com/auth/plus.login&redirect_uri=" + 
					gpRedirectUri + "&response_type=code"; 
		}
		
		return "";
	}
	
	public String getTokenPath(String tag, String code) {
		if(tag.equals("vk")) {
			return "https://oauth.vk.com/access_token?client_id=" + 
					vkAppId + "&client_secret=" + vkSecret + 
					"&code=" + code + "&redirect_uri=" + vkRedirectUri;
		}
		
		if(tag.equals("fb")) {
			return "https://graph.facebook.com/oauth/access_token?client_id=" + 
					fbAppId + "&client_secret=" + fbSecret + 
					"&code=" + code + "&redirect_uri=" + fbRedirectUri;
		}
		
		if(tag.equals("gp")) {
			return "https://www.googleapis.com/oauth2/v3/token?client_id=" + 
					gpAppId + "&client_secret=" + gpSecret + 
					"&code=" + code + "&redirect_uri=" + gpRedirectUri + 
					"&grant_type=authorization_code";
		}
		
		return "";
	}
}
