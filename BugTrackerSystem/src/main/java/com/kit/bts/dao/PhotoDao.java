package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Photo;

@Repository
public class PhotoDao extends AbstractDaoImpl<Photo, Integer> {
	
	public PhotoDao() {
		super(Photo.class);
	}
	
}