function getValidatorReg() {
	var val = {
		'login' : function() {
			var ele = $('#login');
			
			if(ele.val() == "") {
				val.errors = true;
				ele.nextAll(".ue:first").fadeOut(300);
				ele.nextAll(".error:first").fadeIn(300);
			} else {
				ele.nextAll(".error:first").fadeOut(300);
				
				if(!control(ele, "check-login")) {
					val.errors = true;
					ele.nextAll(".error:first").fadeOut(300);
				}
			}
		},
		
		'password' : function() {
			var ele = $('#pass');
			
			if(ele.val() == "") {
				val.errors = true;
				ele.nextAll(".error:first").fadeIn(300);
			} else {
				ele.nextAll(".error:first").fadeOut(300);
			}
		},
		
		'mail' : function() {
			var ele = $('#mail');
			
			if(ele.val() == "") {
				val.errors = true;
				ele.nextAll(".ue:first").fadeOut(300);
				ele.nextAll(".error:first").fadeIn(300);
			} else {
				ele.nextAll(".error:first").fadeOut(300);
				
				if(!control(ele, "check-mail")) {
					val.errors = true;
					ele.nextAll(".error:first").fadeOut(300);
				}
			}
		},
		
		'sendIt' : function (e) {
			if(!val.errors) {
				$("#reg-form").submit();
				e.preventDefault();
			} else {
				e.preventDefault();
			}
		}
	};
	
	return val;
};

var control = function(input, url) {
	
	$.get(url + "?data=" + $(input).val(), function(data) {
		if(data == true) {
			input.nextAll(".ue:first").fadeOut(300);
		} else {
			input.nextAll(".ue:first").fadeIn(300);
		}
	});
	
	return !input.nextAll(".ue:first").is(":visible");
};