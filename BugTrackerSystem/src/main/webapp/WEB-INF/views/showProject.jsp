<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><spring:message code="label.showProjectPageTitle" /></title>
	
	<link href="<c:url value='/resources/css/bootstrap.min.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/projectsPage.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/common.css' />" rel="stylesheet">
	<link href="<c:url value='/resources/css/font-awesome.min.css' />" rel="stylesheet">
	
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
</head>
  <body>
  	<noscript>
  		<meta http-equiv="refresh" content="0;url=/bts/bad-browser">
	</noscript>
  	<div class="container-fluid">
  		<div class="row">
  			<%@ include file="common/header.jspf" %>	
  		</div>
  		
  		<div class="modal fade" id="bug-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			  <div class="modal-content">
			    <div class="modal-header">
			    	<h4><spring:message code='label.bugCreating' />
			    		<button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
			    	</h4>
			    </div>
			    <div class="modal-body"></div>
			  </div>
			</div>
		</div>
  		
  		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 cont">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 projects-data">
					<p class="tip hidden"><spring:message code='label.noAccess' /></p>
				</div>
				
				<c:if test="${addBug ne 0}">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 project-utils">
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 add-bug" data-url="/bts/add-bug/0?p=${project.projectId}">
								<button class="btn btn-success btn-block" style="margin-bottom: 5px" >
									<spring:message code='label.reportABug' />
								</button>
							</div>
							
							<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 search-bug">
								<form class="form-inline pull-right" role="search">
									<div class="form-group">
										<label for="search-projects" style="color: white;"><spring:message code='label.filterBugs' /></label>
										<input type="text" class="form-control search-bugs" placeholder="<spring:message code='label.filter' />">
									</div>
									<button type="button" class="btn btn-default pull-right cancel-filter"><span class="fa fa-remove"></span></button>
									<!-- <span class="text-danger">Параметри пошуку <span class="caret"></span></span> -->
								</form>
			 				</div>
			 			</div>
			 		</div>
			 	</c:if>
			 		
				<c:if test="${not empty bugs}">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bugs" style="background: #ccc;">
						<table class="table bugs-table">
							<thead>
								<tr>
									<th>№</th>
									<th><spring:message code='label.summary' /></th>
									<th><spring:message code='label.priority' /></th>
									<th><spring:message code='label.status' /></th>
									<th><spring:message code='label.bugOptions' /></th>
								</tr>
							</thead>
							
							<tbody>
								<c:forEach items="${bugs}" var="bug" varStatus="i">
									<tr>
										<td>${i.index + 1}</td>
										<td>${bug.summary}</td>
										<td class="${bug.priority}">${bug.priority}</td>
										<td>${bug.status}</td>
										<td><a href="/bts/show-bug/${bug.bugId}" class="dark">
												<spring:message code='label.viewBug' />
											</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</c:if>
			</div>
  		</div>
  		
  		<div class="row">
  			<%@ include file="common/footer.jspf" %>
  		</div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="<c:url value='/resources/js/jquery-1.11.2.min.js' />"></script>
    <script src="<c:url value='/resources/js/jquery.autocomplete.js' />"></script>
    <script src="<c:url value='/resources/js/bootstrap.min.js' />"></script>
    <script src="<c:url value='/resources/js/showProjectPage.js' />"></script>
  </body>
</html>