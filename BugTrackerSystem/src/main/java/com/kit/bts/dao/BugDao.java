package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Bug;

@Repository
public class BugDao extends AbstractDaoImpl<Bug, Integer> {
	
	public BugDao() {
		super(Bug.class);
	}
	
}