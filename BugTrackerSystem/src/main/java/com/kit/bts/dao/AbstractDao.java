package com.kit.bts.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;

public interface AbstractDao<E, I extends Serializable> {

    public E findById(I id);
    public void save(E e);
    public void update(E e);
    public void delete(E e);
    public List<E> getAllEntity();
    
    public Set<E> executeCriterionSet(Criterion criterion, Order order, int maxResults);
    public List<E> executeCriterionList(Criterion criterion, Order order, int maxResults);
}
