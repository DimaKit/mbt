package com.kit.bts.social;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class RequestSender {
	
	public static String sendPost(String url) throws Exception {
		 
		URL obj = new URL(url);
		StringBuffer response = new StringBuffer();
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
 
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setChunkedStreamingMode(8 * 1024);
		con.setDoOutput(true);
 
		if(con.getResponseCode() == HttpsURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
		}  else {
			System.out.println("Bad request " + con.getResponseCode());
		}
		System.out.println(response.toString());
		return response.toString();
	}
	
//	public static String sendPostFB(String url) throws Exception {
//		 
//		URL obj = new URL(url);
//		StringBuffer response = new StringBuffer();
//		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
// 
//		con.setRequestMethod("GET");
//		con.setChunkedStreamingMode(8 * 1024);
//		con.setDoOutput(true);
// 
//		if(con.getResponseCode() == HttpsURLConnection.HTTP_OK) {
//			BufferedReader in = new BufferedReader(
//			        new InputStreamReader(con.getInputStream()));
//			String inputLine;
//	 
//			while ((inputLine = in.readLine()) != null) {
//				response.append(inputLine);
//			}
//			in.close();
//		}  else {
//			System.out.println("Bad request " + con.getResponseCode());
//		}
//		System.out.println(response.toString() + con.getResponseCode());
//		return response.toString();
//	}
}
