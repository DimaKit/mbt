package com.kit.bts.social;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class ResponseParser {
	
	public static String[] getValueFromJson(String json, String[] necessaryFields) {
		String[] values = new String[necessaryFields.length];
		System.out.println(json);
		ObjectMapper m = new ObjectMapper();
		JsonNode actualObj = null;
		
		try {
			actualObj = m.readTree(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < necessaryFields.length; i++) {
			if(actualObj.has(necessaryFields[i])) {
				values[i] = actualObj.get(necessaryFields[i]).asText();
			} else {
				values[i] = "0";
			}
		}
		
		return values;
	}
	
	public static String[] getValueFromParams(String commonParams, String[] necessaryFields) {
		String[] values = new String[necessaryFields.length];
		values[0] = commonParams.substring(0, commonParams.indexOf("&expires"));

		return values;
	}
}
