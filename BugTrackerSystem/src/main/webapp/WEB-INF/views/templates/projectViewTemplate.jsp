<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 single-project">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title">
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 name"></div>
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 webSite"></div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="col-xs-12 col-sm-9 col-md-10 col-lg-10 description"></div>
		<div
			class="col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0 col-md-2 col-md-offset-0 col-lg-2 language"></div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add-info">
		<hr>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 company">
			<i class="fa fa-copyright"></i>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 privacy">
			<i class="fa fa-shield"></i>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 created">
			<i class='fa fa-clock-o'></i>
		</div>
	</div>
</div>
