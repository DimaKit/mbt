package com.kit.bts.dao;

import org.springframework.stereotype.Repository;

import com.kit.bts.entities.Language;

@Repository
public class LanguageDao extends AbstractDaoImpl<Language, Integer> {

	public LanguageDao() {
		super(Language.class);
	}

}
