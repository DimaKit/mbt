jQuery(function($) {
	var fields = {
			"name": "/bts/show-project/{projectId}", 
			"description": "text", 
			"privacy": "text", 
			"created": "date", 
			"webSite": "{webSite}", 
			"language": "image", 
			"company": "text"
		},
	    container = $(".projects-data"),
	    templateURL = "/bts/templates/pvt";
	
	var urlPath = window.location.href.split('/');
	var id = urlPath[urlPath.length - 1];
	
	if(id != "projects") {
		$.getJSON("/bts/get-projects/" + id, function(projects) {
			if(projects.length === 0) {
				container.append("<p class='tip'>No projects yet</p>");
			} else {
				$.each(projects, function(key, project) {
					fillTemplate(container, templateURL, project, fields, key);
				});
			}
		});
	} else {
		$.getJSON("/bts/get-projects", function(projects) {
			if(projects.length === 0) {
				container.append("<p class='tip'>No projects yet</p>");
			} else {
				$.each(projects, function(key, project) {
					fillTemplate(container, templateURL, project, fields, key);
				});
			}
		});
	}
	
	$("#search-projects").keyup(function() {
		$(".single-project").show();
		$('.name:not(:contains(' + $(this).val() + '))').closest(".single-project").hide();
	});
	
	$("#cancel-filter").click(function() {
		$("#search-projects").val("");
		$(".single-project").show();
	});
	
	$(".add-project").click(function() {
		addProject($(this));
	});
});

var addProject = function(container) {
	$(".modal-body").load(container.attr("data-url"), function(data) {
		$('#project-modal').modal('show');
		$(".ug").hide();
		
		$("#users-label").click(function() {
			$(".ug").toggle(function() {});
		});
		
		$(".remove-user").click(function(e) {
    		$(this).closest("tr").remove();
    		e.preventDefault();
    	});
		
		$("#find-user").autocomplete({
		    serviceUrl: "/bts/get-user",
		    onSelect: function (suggestion) {
		    	var idFieldHtml = "<td><input type='hidden' name='ids' value='" + suggestion.data + "'></td>";
		    	var rowsNum 	= "<td>" + $("#users tr").size() + "</td>";
		    	var names 		= "<td>" + suggestion.value + "</td>";
		    	var login 		= "<td>" + suggestion.login + "</td>";
		    	var remove		= "<td><a href='#' class='remove-user dark'>Remove</a></td>";
		        $("#users").append("<tr>" + rowsNum + names + login + remove + idFieldHtml + "</tr>");
		        
		        $(".remove-user:last").click(function(e) {
		    		$(this).closest("tr").remove();
		    		e.preventDefault();
		    	});
		        
		        $("#find-user").val("");
		    },
		
			delimiter: ",",
			transformResult: function(response) {
			   return {
			      suggestions: $.map($.parseJSON(response), function(item) {  
			    	  return { value: item.name + " " + item.sname, data: item.userId.toString(), login: item.login};
			      })
			   };
			}
		});
	});
};

var removeProject = function(container) {
	if(confirm("Delete?")) {
		$.ajax({
			url: container.attr("data-url"),
			type: "post",
			success: function() {
				container.closest(".single-project").hide();
			}
		});
	}
};

var fillTemplate = function(container, templateURL, entity, fields, key) {
	$.get(templateURL, packData);

	function packData(data) {
		container.append(data);
		
		$.each(fields, function(key, val) {
			if(entity.hasOwnProperty(key)) {
				if(val === "text") {
					$(".single-project:last div." + key).append(" " + entity[key]);
				}
				
				if(val === "image") {
					var pathToImg = "/bts/resources/img/" + entity[key] + ".png";
					$(".single-project:last div." + key).html(
						"<img alt='" + entity[key] + "' src='" + pathToImg + "' class='img-responsive img-rounded'>"
					);
				}
				
				if(val.indexOf("/") === 0 || val.indexOf("{") === 0) {
					var pathVar = val.substring(val.indexOf("{") + 1, val.lastIndexOf("}"));
					var href = "<a href='" + val.replace(val.substring(val.indexOf("{"), val.length), entity[pathVar]) + 
							   "' class='dark'>" + entity[key] + "</a>";
					$(".single-project:last div." + key).html(href);
				}

				if(val === "date") {
					$(".single-project:last div." + key).append(
						 " " + new Date(entity[key]).toLocaleDateString());
				}
			}
		});
		
		$("<i/>", {
			"class": 'fa fa-ban fa-2x delete',
			"data-url": '/bts/delete-project/' + entity["projectId"],
			click: function() {
				removeProject($(this));
			}
		}).prependTo(".single-project:last .name");
		
		$("<i/>", {
			"class": 'fa fa-pencil fa-2x update',
			"data-url": '/bts/add-project/' + entity["projectId"],
			click: function() {
				addProject($(this));
			}
		}).prependTo(".single-project:last .name");
	}
};