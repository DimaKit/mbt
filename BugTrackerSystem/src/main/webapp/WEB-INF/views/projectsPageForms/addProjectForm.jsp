<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 add-project">
	<form:form method="post" modelAttribute="newProject" action="/bts/add-project">
		<form:input path="projectId" type="hidden" id="pid" />
		<form:input path="created" type="hidden" />
	
		<div class="form-group">
			<label for="name"><spring:message code='label.prName' /></label>
			<form:input type="text" path="name" id="name" name="name"
				class="form-control" />
		</div>

		<div class="form-group">
			<label for="description"><spring:message code='label.prDesc' /></label>
			<form:textarea type="text" path="description" id="description"
				name="description" rows="5" class="form-control"
				style="resize: none" />
		</div>
		
		<label for="ug" id="users-label"><spring:message code='label.prUsers' /> <span class="caret"></span></label>
		<div class="form-group ug">
			<table class="table" id="users">
				<tr>
					<th>№</th>
					<th><spring:message code='label.name' /></th>
					<th><spring:message code='label.login' /></th>
					<th><spring:message code='label.prOptions' /></th>
					<th>&nbsp;</th>
				</tr>
				
				<c:forEach items="${newProject.users}" var="user" varStatus="i">
					<tr>
						<td>${i.index + 1}</td>
						<td>${user.name} ${user.sname}</td>
						<td>${user.login}</td>
						<c:choose>
							<c:when test="${pageContext.request.userPrincipal.name ne user.login}">
								<td><a href="#" class="remove-user dark"><spring:message code='label.removeUserFromProject' /></a></td>
							</c:when>
							<c:otherwise>
								<td>&nbsp;</td>
							</c:otherwise>
						</c:choose>
						<td><input type="hidden" name="ids" value="${user.userId}"></td>
					</tr>
				</c:forEach>
			</table>
			
			<label for="find-user"><spring:message code='label.findUser' /></label>
			<input type="text" class="form-control" id="find-user" />
		</div>

		<div class="form-group">
			<label for="company"><spring:message code='label.prCompany' /></label>
			<form:input type="text" path="company" id="company" name="company"
				class="form-control" />
		</div>

		<div class="form-group">
			<label for="webSite"><spring:message code='label.prWebSite' /></label>
			<form:input type="text" path="webSite" id="webSite" name="webSite"
				class="form-control" />
		</div>

		<div class="form-group">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pr-lang">
				<label for="language"><spring:message code='label.prLang' /></label>
				<form:select path="language" id="language" name="language"
					class="form-control">
					<c:forEach items="${langs}" var="l">
						<form:option value="${l.langName}" class="form-control">${l.langName}</form:option>
					</c:forEach>
				</form:select>
			</div>

			<label for="privacy" class="pr-privacy-lbl"><spring:message code='label.prPrivacy' /></label><br>
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="privacy">
				<label for="privacy-public"><spring:message code='label.prPrivacy.public' /></label>
				<form:radiobutton value="Public" path="privacy" id="privacy-public" name="privacy" checked="checked" /><br>
				
				<label for="privacy-private"><spring:message code='label.prPrivacy.private' /></label>
				<form:radiobutton value="Private" path="privacy" id="privacy-private" name="privacy" />
			</div>
		</div>
	
		<input type="submit" value="<spring:message code='label.confirm' />" class="btn btn-primary btn-block" />
	</form:form>
</div>
