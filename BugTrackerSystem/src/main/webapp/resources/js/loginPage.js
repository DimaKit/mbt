function windowSize() {
	if ($(window).width() < '975') {
		$('.form').detach().insertBefore('.stats');
	} else {
		$('.form').detach().insertAfter('.left-container');
	}
}

$(window).load(windowSize); 
$(window).resize(windowSize);

$(document).ready(function() {
	getStats();
	
//	setInterval(getStats, 30000);
	slider();
	switchForm("login-form");
	
	
});

function slider() {
	var counter = 0;
	
	setInterval(function() {
		counter++;

		if(counter == $(".title").size()) {
			counter = 0;
		}
		
		$('.slider').fadeOut(400, function() {
			setVisibility(".title", counter);
			setVisibility(".image", counter);
			setVisibility(".arguments", counter);
			
			$(this).fadeIn(400);
		});
	}, 5000);
} 

function setVisibility(cl, shown) {
	$(".left-container").find(cl).each(function(i) {
		if(i == shown) {
			$(this).removeClass("hidden");
		} else {
			$(this).addClass("hidden");
		}
	});
}

function switchForm(url) {
	$(".form").fadeOut(200, function() {
		$(this).empty();
		$(this).load("/bts/" + url, function() {
			if(url === "login-form") {
				var lVal = getValidator();
				
				$("#confirm").click(function(e) {
					lVal.errors = false;
					lVal.login();
					lVal.password();
					lVal.sendIt(e);
				});

				$('#login').keyup(lVal.login);
				$('#pass').keyup(lVal.password);

			} else {
				var rVal = getValidatorReg();

				$("#confirm").click(function(e) {
					rVal.errors = false;
					rVal.login();
					rVal.password();
					rVal.mail();
					rVal.sendIt(e);
				});

				$('#login').keyup(rVal.login);
				$('#pass').keyup(rVal.password);
				$('#mail').keyup(rVal.mail);
			}
			
			$(this).fadeIn();
		});
	});
}

function getStats() {
	$.getJSON("/bts/stats", function(response) {
		$(".us-val, .pr-val").empty();
		$(".us-val").append("<h4>" + response[0] + "</h4>");
		$(".pr-val").append("<h4>" + response[1] + "</h4>");
	});
}
