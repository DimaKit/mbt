package com.kit.bts.service;

import java.util.Comparator;

import com.kit.bts.entities.Report;

public class ComparatorFactory {

	public static Comparator<Report> getDefaultComparator() {
		return new Comparator<Report>() {
			@Override
			public int compare(Report o1, Report o2) {
				return Integer.compare(o2.getReportId(), o1.getReportId());
			}
		};
	}
	
	public static Comparator<Report> getStatusComparator() {
		return new Comparator<Report>() {
			@Override
			public int compare(Report o1, Report o2) {
				return o1.getStatus().compareTo(o2.getStatus());
			}
		};
	}
	
}
