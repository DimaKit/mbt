package com.kit.bts.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.ProjectDao;
import com.kit.bts.entities.Project;
import com.kit.bts.entities.User;

@Service
@Transactional
public class ProjectService {
	
	@Autowired
	private ProjectDao dao;
	
	@Autowired
	private UserService us;
	
	@Autowired
	private ReportService rs;
	
	public Project findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Project e, Locale locale, User user) {
		e.setCreated(new Date());
		
		dao.save(e);
		
		rs.createReportOnProjectSaving(e, locale, user);
	}

	public void update(Project e) {
		dao.update(e);
	}

	public void delete(Project e, Locale locale, User user) {
		dao.delete(e);
		
		rs.createReportOnProjectRemoving(e, locale, user);
	}

	public List<Project> getAllEntity() {
		return dao.getAllEntity();
	}
	
	public int getProjectsQuantity() {
		return getAllEntity().size();
	}
	
	public List<Project> getLastPublicProjects() {
		Criterion c = Restrictions.eq("privacy", "public");
		
		List<Project> projects = dao.executeCriterionList(c, Order.desc("projectId"), 5);
		Project mBT = findById(1); // mBT
		
		if(!projects.contains(mBT)) {
			projects.add(mBT);
		}
		
		return projects;
	}
	
	public void setUsersToProject(Project p, int[] ids) {
		List<User> users = new ArrayList<User>();
		User user = null;
		
		for (int i : ids) {
			user = us.findById(i);
			
			if(i > 0 && !users.contains(user)) {
				users.add(user);
			}
		}
		
		p.setUsers(users);
	}
}
