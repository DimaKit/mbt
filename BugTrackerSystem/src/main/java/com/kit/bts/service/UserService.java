package com.kit.bts.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.UserDao;
import com.kit.bts.entities.Project;
import com.kit.bts.entities.User;

@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserDao dao;

	public User findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(User e) {
		e.setEnabled(true);
		e.setRole("ROLE_USER");
		
		dao.save(e);
	}

	public void update(User e) {
		dao.update(e);
	}

	public void delete(User e) {
		dao.delete(e);
	}

	public List<User> getAllEntity() {
		return dao.getAllEntity();
	}
	
	public User getUserByCrendetials(String login, String password) {
		Criterion c = Restrictions.and(Restrictions.eq("login", login), Restrictions.eq("password", password));
		return dao.executeCriterionList(c, null, 0).isEmpty() ? null : (User) dao.executeCriterionList(c, null, 0).get(0);
	}

	public User getUserByLogin(String login) {
		Criterion c = Restrictions.eq("login", login);
		return dao.executeCriterionList(c, null, 0).isEmpty() ? null : (User) dao.executeCriterionList(c, null, 0).get(0);
	}
	
	public int getUsersQuantity() {
		return getAllEntity().size();
	}
	
	public boolean isLoginEnabled(String login) {
		Criterion c = Restrictions.eq("login", login);
		
		return dao.executeCriterionSet(c, null, 0).size() == 0;
	}
	
	public boolean isMailEnabled(String mail) {
		Criterion c = Restrictions.eq("mail", mail);
		
		return dao.executeCriterionSet(c, null, 0).size() == 0;
	}

	public List<User> getUsersByNameAndSname(String query) {
		String name, sname;
		Criterion c = null;
		
		if(query.indexOf(' ') >= 0) {
			name = query.substring(0, query.indexOf(' '));
			sname = query.substring(query.indexOf(' ') + 1, query.length());
			c = Restrictions.and(Restrictions.eq("name", name), Restrictions.like("sname", sname + "%"));
		} else {
			name = query;
			sname = "";
			c = Restrictions.like("name", name + "%");
		}
		
		return dao.executeCriterionList(c, null, 0);
	}
	
	public Set<Project> getPublicProjects(User user) {
		Set<Project> projects = user.getProjects();
		Set<Project> publicProjects = new HashSet<Project>();
		
		for(Project p: projects) {
			if(p.getPrivacy().equals("Public")) {
				publicProjects.add(p);
			}
		}
		
		return publicProjects;
	}
}
