package com.kit.bts.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kit.bts.dao.ReportDao;
import com.kit.bts.entities.Bug;
import com.kit.bts.entities.Project;
import com.kit.bts.entities.Report;
import com.kit.bts.entities.User;

@Service
@Transactional
public class ReportService {
	
	@Autowired
	private ReportDao dao;
	
	@Autowired
	@Qualifier("messageSource")
	private MessageSource messageSource;
	
	private String host = "smtp.ukr.net",
				   username = "v3il@ukr.net",
				   password = "28121994",
				   subject = "New bug was created";
	
	public Report findById(Integer id) {
		if(id > 0) {
			return dao.findById(id);
		} else {
			return null;
		}
	}

	public void save(Report e) {
		dao.save(e);
	}

	public void update(Report e) {
		dao.update(e);
	}

	public void delete(Report e) {
		dao.delete(e);
	}

	public List<Report> getAllEntity() {
		return dao.getAllEntity();
	}
	
	public List<Report> getNewReports(User user, int size) {
		List<Report> ordered = new ArrayList<Report>();
		List<Report> userReports = user.getNews();
		Collections.reverse(userReports);
		
		for(Report r: userReports) {
			if(r.getStatus().equals("new") && ordered.size() < size) {
				ordered.add(r);
			}
		}
		
		Collections.sort(ordered, ComparatorFactory.getDefaultComparator());
		Collections.reverse(userReports);
		return ordered;
	}
	
	public List<Report> getLastNews(User user, int size) {
		List<Report> ordered = new ArrayList<Report>();
		List<Report> userReports = user.getNews();
		Collections.reverse(userReports);
		
		for(Report r: userReports) {
			if(ordered.size() < size) {
				ordered.add(r);
			}
		}
		
		Collections.sort(ordered, ComparatorFactory.getDefaultComparator());
		return ordered;
	}
	
	public void changeStatusOfNewReports(User user, int size) {
		for(Report r: getNewReports(user, size)) {
			r.setStatus("old");
			update(r);
		}
	}
	
	public void createReportOnProjectSaving(Project p, Locale locale, User user) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		Report r = new Report(
				messageSource.getMessage("label.createProjectReport", new Object[] {
						sdf.format(p.getCreated()), 
						p.getName(), 
						p.getLanguage(), 
						p.getPrivacy(),
						p.getProjectId()
				}, locale), "new", user);
		
		dao.save(r);
	}
	
	public void createReportOnProjectRemoving(Project p, Locale locale, User user) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		Report r = new Report(
				messageSource.getMessage("label.removeProjectReport", new Object[] {
						sdf.format(new Date()), 
						p.getName(), 
						p.getLanguage(), 
						p.getPrivacy()
				}, locale), "new", user);
		
		dao.save(r);
	}
	
	public void createReportOnBugCreating(Bug b, Locale locale, User user, MailSender sender) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		String msg = messageSource.getMessage("label.createBugReport", new Object[] {
				sdf.format(b.getCreated()), 
				user.getLogin(), 
				b.getProject().getName(), 
				b.getPriority(),
				b.getBugId()
			}, locale);
			
		Report r = new Report(msg, "new", user);
		dao.save(r);
		
		sendMail(msg, sender, b.getProject().getUsers().get(0).getMail());
	}
	
	public void sendMail(String message, MailSender sender, String to) {
		
		Properties props = new Properties();

	    props.put("mail.smtp.starttls.enable", "true");
	    
	    props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.socketFactory.port", "465");
	     
	    props.put("mail.smtp.ssl.trust", host);
	    props.put("mail.smtp.host", host); 
	    props.put("mail.smtp.port", "465");
	    props.put("mail.smtp.auth", "true");

		Message msg = new MimeMessage(Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				}));
		try {
			msg.setSubject(subject);
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to,
					false));
			msg.setFrom(new InternetAddress(username));
			msg.setContent(message, "text/html; charset=utf-8");
			msg.setSentDate(new Date());
			Transport.send(msg);
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}
}
