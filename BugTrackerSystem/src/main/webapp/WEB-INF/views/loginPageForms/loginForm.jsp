<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 login-form">
	<h4 class="h4">
		<spring:message code='label.enterTheSystem' />
		<i class="fa fa-user-plus switch-form"
			onclick="switchForm('registration-form');"></i>
	</h4>
	<hr>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
		<p>
			<spring:message code='label.noSuchUser' />
		</p>
	</div>
	<form method="POST" action="login-form">
		<label for="login"><spring:message code="label.login" /></label> <input
			type="text" name="login" class="form-control" id="login"
			autofocus="autofocus" />

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
			<p>
				<spring:message code="label.cantBeEmpty" />
			</p>
		</div>

		<label for="pass"><spring:message code="label.password" /></label> <input
			type="password" name="pass" class="form-control" id="pass" />

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error">
			<p>
				<spring:message code="label.cantBeEmpty" />
			</p>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 push-down">
			<label for="remem"><spring:message code="label.remember" /></label>
			<input type="checkbox" id="remem" name="remem" checked="checked" />
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 push-down">
			<input type="submit" class="btn btn-success btn-block"
				value="<spring:message code="label.confirm" />" id="confirm">
		</div>
	</form>
</div>